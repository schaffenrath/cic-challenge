package com.schaffenrath.DataHandling;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataProcessing {

	private final String[] targetProperties = { "department", "source_type", "emissions_mtco2e" };

	// @VisibleForTesting
	protected final JSONArray filterAndConvertResponseToJSON(String requestedDepartmentData) throws JSONException {
		JSONArray filteredDepartmentData = new JSONArray();
		if (requestedDepartmentData == null || requestedDepartmentData.isBlank())
			return filteredDepartmentData;
		JSONArray receivedDepartmentData = new JSONArray(requestedDepartmentData);
		// Filter out the target properties for each entry from the received data
		receivedDepartmentData.forEach(departmentEntry -> {
			JSONObject receivedObj = (JSONObject) departmentEntry;
			JSONObject processedObj = new JSONObject();
			for (String property : this.targetProperties) {
				processedObj.put(property, receivedObj.get(property));
			}
			filteredDepartmentData.put(processedObj);
		});
		return filteredDepartmentData;
	}

	// @VisibleForTesting
	protected final JSONArray requestAndProcessDepartmentCo2Data(String departmentStartsWith) {
		DataAcquisition dataAcquisition = new DataAcquisition();
		String departmentStringData = dataAcquisition.requestDepartmentCo2Data(departmentStartsWith);
		JSONArray filteredDepartmentJSONData = new JSONArray();
		try {
			filteredDepartmentJSONData = filterAndConvertResponseToJSON(departmentStringData);
		} catch (JSONException e) {
			System.err.println("Failed to convert API response to JSON array: " + e.getMessage());
		}
		return filteredDepartmentJSONData;
	}

	public final JSONArray getFilteredDepartmentJSON(String departmentStartsWith) {
		return requestAndProcessDepartmentCo2Data(departmentStartsWith);
	}

}
