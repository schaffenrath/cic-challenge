package com.schaffenrath.Rest;

import com.schaffenrath.DataHandling.DataProcessing;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("department")
public class SfCo2EmissionService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIt(@QueryParam("departmentStartsWith") String departmentStartsWith) {
		DataProcessing dataProcessing = new DataProcessing();
		return Response.status(200).header("Access-Control-Allow-Origin", "*")
				.entity(dataProcessing.getFilteredDepartmentJSON(departmentStartsWith).toString()).build();
	}
}
