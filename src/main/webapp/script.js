let receivedCo2Data;

function createTableFromJson(jsonData) {
	let col = ['department', 'source_type', 'emissions_mtco2e'];
	let table = document.createElement("table");
	let tr = table.insertRow(-1);
	
	for (let i=0; i < col.length; i++) {
		let th = document.createElement("th");
		th.innerHTML = col[i];
		tr.appendChild(th);
	}
	
	for (let i=0; i < jsonData.length; i++) {
		tr = table.insertRow(-1);
		for (let j=0; j < col.length; j++) {
			let cell = tr.insertCell(-1);
			cell.innerHTML = jsonData[i][col[j]];
		}
	}
	
	const divContainer = document.getElementById("tableData");
	divContainer.innerHTML="";
	divContainer.appendChild(table);
}


function requestCo2Data() {
	let requestURL = "http://rsbpi.ddns.net:8080/CICChallenge/webapi/department";
	const filterElement = document.getElementById("departmentFilter");
	const filterValue = filterElement.value.trim();
	if (filterValue) {
		requestURL = requestURL + "?departmentStartsWith=" + filterValue;
	}
	fetch(requestURL)
	.then(response => response.json())
	.then(data => {createTableFromJson(data)})
	.catch((error) => console.error('Failed to get data: '+ error));
}
