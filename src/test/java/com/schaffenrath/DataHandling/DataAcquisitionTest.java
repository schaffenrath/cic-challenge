package com.schaffenrath.DataHandling;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DataAcquisitionTest {

	private DataAcquisition dataAcquisition = new DataAcquisition();

	@DisplayName("Test building API request URL containing constrains")
	@Test
	void testBuildAPIRequestURL() throws MalformedURLException {
		String buildURLString = this.dataAcquisition.buildAPIRequestURL("Abc").toString();
		assertTrue(buildURLString.contains("starts_with(department,%22Abc%22)"));
		assertTrue(buildURLString.contains("emissions_mtco2e%3E0"));
		assertTrue(buildURLString.contains("app_token="));
	}

	@DisplayName("Test API request URL without filter")
	@Test
	void testAPIRequestURLWithoutFilter() throws MalformedURLException {
		String buildURLString = this.dataAcquisition.buildAPIRequestURL(null).toString();
		assertFalse(buildURLString.contains("starts_with"));
	}

	@DisplayName("Test invalid API request")
	@Disabled("Temp disabled, since it is making requests to endpoint")
	@Test
	void testInvalidAPIRequest() {
		assertEquals("Failed to retrieve data!", this.dataAcquisition.requestDepartmentCo2Data("? @"));
	}

	@DisplayName("Test received CO2 data department constraint")
	@Disabled("Temp disabled, since it is making requests to endpoint")
	@Test
	void testDataRequest() {
		String departmentCO2String = this.dataAcquisition.requestDepartmentCo2Data("M");
		JSONArray departmentCO2JSON = new JSONArray(departmentCO2String);
		departmentCO2JSON.forEach(entry -> {
			JSONObject departmentCO2Entry = (JSONObject) entry;
			String department = departmentCO2Entry.getString("department");
			assertTrue(department.startsWith("M"));
		});
	}

}
